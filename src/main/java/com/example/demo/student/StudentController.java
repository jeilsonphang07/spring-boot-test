package com.example.demo.student;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

//API
@RestController
@RequestMapping(path = "api/v1/student")
public class StudentController {

    private final StudentService studentService;

    @Autowired
    public StudentController(StudentService studentService) {
        this.studentService = studentService;
    }

    //GET
    @GetMapping
    public List<Student> getStudents(){
        return studentService.getStudents();
    }

    //POST
    @PostMapping
    public void registerNewStudent(@RequestBody Student student) {      //RequestBody to take response from client and map it to this Student
        studentService.addNewStudent(student);
    }
}
