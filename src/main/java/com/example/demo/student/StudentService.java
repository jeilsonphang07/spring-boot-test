package com.example.demo.student;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class StudentService {

    private final StudentRepository studentRepository;

    @Autowired
    public StudentService(StudentRepository studentRepository) {
        this.studentRepository = studentRepository;
    }


    //GET
    public List<Student> getStudents(){
        return studentRepository.findAll();
    }


    //POST
    public void addNewStudent(Student student) {
        System.out.println(student);
    }
}
